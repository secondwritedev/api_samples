from SecondWriteApi import SecondWriteAPI
import argparse
import os

def submit(args,sw_api):
    for item in args.item_paths:
        if os.path.isdir(item):
            results = sw_api.submit_dir(
                item,
                resubmit=args.resubmit,
                os_type=args.os
            )
            for item, result in results.iteritems():
                yield item,result[0],result[1]
            
            
        elif os.path.isfile(item):
            try:
                status_code,content = sw_api.submit_file(
                    item,
                    resubmit=args.resubmit,
                    os_type=args.os
                )
            except Exception as e:
                status_code,content = -1,str(e)
                
            yield item,status_code,content
        else:
            raise OSError("Could not submit item {}".format(item))
            
def report(args,sw_api):
    status_code,content = sw_api.report(args.sample_hash,report_format=args.format,os_type=args.os)
    if status_code == 200 and args.output:
        with open(args.output,'w') as output:
            output.write(content) 
    return status_code,content
    
def details(args,sw_api):
    status_code,content = sw_api.details(args.sample_hash,detail_type=args.format,os_type=args.os)
    if status_code == 200 and args.output:
        with open(args.output,'w') as output:
            output.write(content)
    return status_code,content
def usage(args,sw_api):
    return sw_api.usage()
    
p = argparse.ArgumentParser()
p.add_argument("api_key",type=str,help="SecondWrite API Key")
subparsers = p.add_subparsers(help='',dest='action')

# Submit Result Parser
p_submit = subparsers.add_parser('submit',help='Submit file to SecondWriteAPI')
p_submit.add_argument("item_paths",type=str,help="path to sample file or directory",nargs="+")
p_submit.add_argument("-r","--resubmit",help="resubmit file (do not use cached report if available)",default=False,action='store_true')
p_submit.add_argument("-s","--os",type=str,help="OS Type to submit to",default="win7")
p_submit.set_defaults(func=submit)

# Report Result Parser
p_report = subparsers.add_parser('report',help='Retrieve report from SecondWrite')
p_report.add_argument("sample_hash",type=str,help="sha256 hash of sample")
p_report.add_argument("-f","--format",type=str,choices=["json","html","slim"],default="json",help="format of report to retrieve")
p_report.add_argument("-o","--output",type=str,default=None,help="Full path to location where retrieved data will be written")
p_report.add_argument("-s","--os",type=str,help="OS Type to submit to",default="win7")
p_report.set_defaults(func=report)

# Details Result Parser
p_details = subparsers.add_parser('details',help='Details from SecondWrite')
p_details.add_argument("sample_hash",type=str,help="sha256 hash of sample")
p_details.add_argument("-f","--format",type=str,choices=["pcap"],default="pcap",help="format of details to retrieve")
p_details.add_argument("-o","--output",type=str,default=None,help="Full path to location where retrieved data will be written")
p_details.add_argument("-s","--os",type=str,help="OS Type to submit to",default="win7")
p_details.set_defaults(func=details)

p_usage = subparsers.add_parser('usage',help='Usage of API key')
p_usage.set_defaults(func=usage)

args = p.parse_args()

sw_api = SecondWriteAPI(args.api_key)

if args.action == "submit":
    for file_path, status_code, content in args.func(args,sw_api):
        print(
            "{},{}".format(
                file_path,
                content
            )
        )
else:
    content = None

    try:
        status_code,content = args.func(args,sw_api)
    except Exception as e:
        print str(e)

    if content:
        print content
