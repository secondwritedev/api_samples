class RateLimitException(Exception):
    """Raise when submission limit is reached"""
    pass
class ResultNotYetReadyException(Exception):
    """Raise when result is not available for a sample"""
    pass
class AccessDeniedException(Exception):
    """Raise for general access exceptions"""
    pass
class ApiKeyHasExpiredException(Exception):
    """Raise when license expiration date has been reached"""
    pass
class ApiKeyIncorrectException(Exception):
    """Raise when api key given is incorrect"""
    pass
class SizeLimitException(Exception):
    """Raise when file exceeds SecondWrite's size limit"""
    pass
class SampleAnalysisFailedException(Exception):
    """Raise when analysis has failed"""
    pass
class HTMLReportNotAvailableException(Exception):
    pass
class ReportNotAvailable(Exception):
    pass
class InternalErrorException(Exception):
    pass
class InvalidInputException(Exception):
    pass
