## Python Code Samples
The files in this directory are Python 2.7 compatible code samples. They include a demo application (demo.py) and a general library for the SecondWrite API (SecondWriteApi.py). Additionally, sw_exceptions.py details possible exceptions encountered by the SecondWrite API.

### SecondWriteAPI library
The SecondWriteAPI library contains implementations of the most common APIs in DeepView. These include, submission of samples, retrieval of reports,retrieval of pcap files, and retrieval of usage statistics for a user.  
To use this, first create a SecondWriteAPI object using the constructor:  
```
    sw_api = SecondWriteAPI([API KEY])
```
After this, use this object to call its various methods. For instance, to submit a file 'example.exe' use the code:  
```
    status_code,content = sw_api.submit_file("/path/to/example.exe")``
```
If the status code is 200, then the content will be the SHA256 sum of the sample. This could then be used to retrieve the report, i.e.:  
```
    time.sleep(300) # Do not begin polling immediately``  
    status_code = -1
    while ( status_code != 200 ):
        status_code,content = sw_api.report("[SHA256SUM"]")
        if (status_code == 200):
           break # Skip sleep
        time.sleep(30)
```

Refer to the docstrings within the file itself for more information about the various features included in this library.

### Demo Application
The demo application is a simple command line interface to the library. It includes ways of accessing all of the features mentioned above.  
Use of this command is of the form:  
```
    python demo.py [API_KEY] [submit,report,details,usage]
```
Each of the above 'actions' has their own individual required arguments

For instance, to submit the sample 'example.exe', the command would be  
```
    python demo.py [API_KEY] submit /path/to/example.exe
```
This should return a SHA256 hash for this sample.


To retrieve a report for this file (after approximately 5 minutes), use:  
```
    python demo.py [API_KEY] report [SAMPLE_SHA256_HASH]
```
This may return an error if the sample has not completed.


To learn more about the demo program, simply type:  
```
    python demo.py --help
```
This gives an overview of the various commands that the demo application accepts.

To learn more about an individual command, use:  
```
    python demo.py [API_KEY] [submit,report,details,usage] --help
```
This will list required arguments, and more options not included in this document.

