#!/usr/bin/env python
import os
import requests
import base64
import hmac
import json
import hashlib

from sw_exceptions import *

from requests.packages.urllib3.util import Retry
from requests.adapters import HTTPAdapter
from requests import Request,Session,exceptions

# Enable retries for requests in case of 504
req_retry = Retry(total=4,backoff_factor=0.3,status_forcelist=[504])
http_adapter = HTTPAdapter(max_retries=req_retry)\

# Constants
API_KEY_LENGTH = 64
FILE_SIZE_LIMIT = 31457280 # 31 MB
DEFAULT_OS = "win7"
DEFAULT_REPORT_TYPE = "json"
DEFAULT_DETAILS_TYPE = "pcap"

# Endpoints
SUBMIT_ENDPOINT = '/submit'
REPORT_ENDPOINT = '/report'
SLIM_REPORT_ENDPOINT = '/slim_report'
DETAILS_ENDPOINT = '/details'
USAGE_ENDPOINT = '/usage'

# Status Codes
HTTP_SAMPLE_LIMIT_REACHED = 429
HTTP_USER_NOT_FOUND = 403
HTTP_EXPIRATION_DATE_REACHED = 402
HTTP_API_KEY_INCORRECT = 401
HTTP_INVALID_INPUT = 400
HTTP_SUCCESS = 200


class SecondWriteAPI():

    def __init__(self,api_key,url="https://api.secondwrite.com"):
        self.api_url = url
        self.api_key = api_key
        
        if not api_key or len(api_key) != API_KEY_LENGTH:
            raise ValueError("API key is malformed")
        
        self.file_size_limit = FILE_SIZE_LIMIT
        self.create_session()

    def create_session(self):
        self.session_obj = Session()
        self.session_obj.mount('http://' , http_adapter)
        self.session_obj.mount('https://', http_adapter)

    def interpret_status(self,response):
        """Interpret the status code returned during a submission and raise
           exceptions if necessary
        
        Args:
                response: Response object from api call
        """
        if (response.status_code == HTTP_SAMPLE_LIMIT_REACHED):
            raise RateLimitException("Sample limit reached")

        if (response.status_code == HTTP_USER_NOT_FOUND):
            raise AccessDeniedException("No user found by that key")

        if (response.status_code == HTTP_EXPIRATION_DATE_REACHED):
            raise ApiKeyHasExpiredException("Key has expired")
            
        if (response.status_code == HTTP_API_KEY_INCORRECT):
            raise ApiKeyIncorrectException("API key specified is incorrect")
        
        if (response.status_code == HTTP_INVALID_INPUT):
            raise InvalidInputException("Input was invalid: {}".format(response.text))
            
    def send_request(self, request, interpret_status=True):
        """Send the request object 
        
        Args:
                request:          (Unprepared) Request object
                interpret_status: Interpret the status code and throw 
                                  appropriate exceptions if True. Otherwise
                                  do nothing.
        Returns:
                status_code: http status code from response
                content: content from response
        """
        
        prepped = request.prepare()
        
        response = self.session_obj.send(prepped)
        
        # If we want to raise exceptions on general status code errors here, 
        # run this function. 
        if interpret_status:
           self.interpret_status(response)
           
        return response

    def submit_dir(self, directory_path, os_type=DEFAULT_OS, resubmit=False):
    
        """Submit all files in a  directory to the DeepView API based on a path.
        
        Args:
                directory_path: string representing the location of the 
                                directory
                os_type:        string representing the os that the 
                                files in the directory should be run on.
                resubmit:       boolean indicating whether or not the file 
                                should be re-run, or if a cached version of the 
                                result should be used
        Returns:
                Dictionary associating file path with tuple of the form:
                
                    status_code: http status code from response
                    content: content from response
        """
        
        results = {}
        
        if not os.path.isdir(directory_path):
            raise OSError(
                "Directory {} does not exist".format(
                    directory_path
                )
            )
            
        for root,dirs,files in os.walk(directory_path):
            for filename in files:
                sample_path = path = os.path.join(root,filename)
                try:
                    return_info = self.submit_file(
                        sample_path,
                        os_type,
                        resubmit
                    )
                except Exception as e:
                    return_info = (-1,str(e))
                
                results[sample_path] = return_info
                
        return results

    def submit_file(self, sample_path, os_type=DEFAULT_OS, resubmit=False):
    
        """Submit a file to the DeepView API based on a file path. 
        
        Args:
                sample_path:  string representing the location of the file
                os_type:      string representing the os that the file should
                              be run on.
                resubmit:     boolean indicating whether or not the file should
                              be re-run, or if a cached version of the result 
                              should be used
        Returns:
                status_code: http status code from response
                content: content from response
        """
        with open(sample_path, 'rb') as submit_file:
            return self.submit_sample(submit_file, os_type, resubmit)
            
    def submit_sample(self, sample, os_type=DEFAULT_OS, resubmit=False):
    
        """Submit a file to the DeepView API based on a file-like object.
        
        Args:
                sample:       file-like object representing the sample to be
                              submitted
                os_type:      string representing the os that the file should
                              be run on.
                resubmit:     boolean indicating whether or not the file should
                              be re-run, or if a cached version of the result 
                              should be used
        Returns:
                status_code: http status code from response
                content: content from response
        """
        
        # Check File Size
        
        sample.seek(0,os.SEEK_END)
        sample_size = sample.tell()
        if sample_size > self.file_size_limit:
            raise SizeLimitException("The file size limit on SecondWrite is {} bytes. Your file is {} bytes".format(str(self.file_size_limit),str(sample_size)))
        sample.seek(0)
        
        url = self.api_url + SUBMIT_ENDPOINT
        request = Request('POST',
                           url,
                           files = {'file' : sample},
                           data = {
                                    'api_key' : self.api_key,
                                    'os'      : os_type,
                                    'resubmit': resubmit
                                  }
                          )
            
        response = self.send_request(request)
        
        
        if (response.status_code == 500):
            raise InternalErrorException("Internal Error Encountered: {}".format(response.text))
            
        return response.status_code,response.content
                                   
    def report(self, sample_hash, report_format=DEFAULT_REPORT_TYPE, os_type=DEFAULT_OS):
    
        """Retrieve a report based on a sample hash, if the file has previously
           been submitted. 
        Args:
                sample_hash:   string representing the SHA256 hash of the 
                report_format: string representing the format of the report to
                               return.
                               Possible values include: json,html,and slim
                os_type:       string representing the os that the file was run
                               on.

        Returns:
                status_code: http status code from response
                content: content from response
        """
    
        parameters = {
                       'sample'  : sample_hash,
                       'api_key' : self.api_key,
                       'os'      : os_type
                     }
        
        if report_format in ["json","html"]:
           url = self.api_url + REPORT_ENDPOINT
           parameters["format"] = report_format  
        elif report_format == "slim":
           url = self.api_url + SLIM_REPORT_ENDPOINT
        else:
            raise ValueError("Invalid report_format: {}".format(report_format))
            
        request = Request('GET',
                           url,
                           params = parameters
                         )
                         
        response = self.send_request(request)
        
        if (response.status_code == 503):
            if "not completed yet" in response.content:
                raise ResultNotYetReadyException("Result not yet ready")
        if (response.status_code == 500):
            if "failed to produce a HTML Report" in response.content:
                raise HTMLReportNotAvailableException("No HTML report is available. JSON report is available")
            elif "not yet been submitted" in response.content or "Could not find data" in response.content:
                raise ReportNotAvailable("Report not available for this sample. Please resubmit this sample.")
            elif "has failed" in response.content:
                raise SampleAnalysisFailedException("Sample analysis has failed")
            else:
                raise InternalErrorException("Internal Error Encountered")
        
        content = response.content
        
        if response.status_code == HTTP_SUCCESS:
            if report_format in ["json","slim"]:
                content = json.dumps(response.json())
        
        
        return response.status_code,content
    
    def details(self, sample_hash, detail_type=DEFAULT_DETAILS_TYPE, os_type=DEFAULT_OS):

        """Retrieve analysis details based on a sample hash, if the file has 
           previously been submitted. Currently PCAP files are the only
           supported files
        Args:
                sample_hash:   string representing the SHA256 hash of the 
                detail_type:   string representing the type of detail to
                               return
                os_type:       string representing the os that the file was run
                               on.

        Returns:
                status_code: http status code from response
                content: content from response
        """

        if not detail_type == "pcap":
            raise ValueError("Invalid detail_type: {}".format(detail_type))
        
        url = self.api_url + DETAILS_ENDPOINT
            
        request = Request('GET',
                           url,
                           params  = {
                                       'sample'  : sample_hash,
                                       'api_key' : self.api_key,
                                       'type'    : detail_type,
                                       'os'      : os_type
                                     }
                          )

        response = self.send_request(request)
    
        if (response.status_code == 503):
            if "not completed yet" in response.content:
                raise ResultNotYetReadyException("Result not yet ready")
        if (response.status_code == 500):
            if "not yet been submitted" in response.content or "Could not find data" in response.content:
                raise ReportNotAvailable("Details not available for this sample. Please resubmit this sample.")
            elif "has failed" in response.content:
                raise SampleAnalysisFailedException("Sample analysis has failed")
            else:
                raise InternalErrorException("Internal Error Encountered")
         
        return response.status_code,response.content
        
    def usage(self):

        """Retrieve API usage information (files submitted)
        Returns:
                status_code: http status code from response
                content:     content from response
        """
    
        url = self.api_url + USAGE_ENDPOINT
            
        request = Request('GET',
                           url,
                           params  = {'api_key' : self.api_key}
                         )
                        
        response = self.send_request(request)
                         
        if (response.status_code == 500):
            raise InternalErrorException("Internal Error Encountered")
            
        return response.status_code,response.text
