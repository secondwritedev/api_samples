﻿using SecondWriteApi;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Demo
{
    class Program
    {
        const string ApiKeyEnvVar = "SW_API_KEY";

        private static readonly string SUBMIT_FLAG = "--submit-file";
        private static readonly string RETRIEVE_FLAG = "--retrieve-report";
        private static readonly string FULL_REPORT_FLAG = "--full";

        static string GetApiKey()
        {
            var apiKey = Environment.GetEnvironmentVariable(ApiKeyEnvVar);

            if (apiKey == null)
            {
                Console.Error.WriteLine("CONFIG ERROR: {0} key not found in environment", ApiKeyEnvVar);
                Environment.Exit(-5);
            }

            var regex = @"[A-Fa-f0-9]{64}";
            var match = Regex.Match(apiKey, regex, RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                Console.Error.WriteLine("Please check your {0} environment variable. It doesn't look right.", ApiKeyEnvVar);
                Environment.Exit(-6);
            }

            return apiKey;
        }

        static void PrintHelp()
        {
            Console.Error.WriteLine($"Demo.exe {SUBMIT_FLAG} <file_path>");
            Console.Error.WriteLine($"Demo.exe {RETRIEVE_FLAG} <token> [{FULL_REPORT_FLAG}]");
        }

        static async Task Main(string[] args)
        {
            if (args == null || !(args.Length == 2 || args.Length == 3))
            {
                PrintHelp();
                Environment.Exit(-1);
            }

            if (!args[0].Equals(SUBMIT_FLAG) && !args[0].Equals(RETRIEVE_FLAG))
            {
                PrintHelp();
                Console.Error.WriteLine("Invalid flag: '{0}'.", args[0]);                
                Environment.Exit(-2);
            }

            var api = new SecondWrite(GetApiKey());

            if (args[0].Equals(SUBMIT_FLAG))
            {
                if(!File.Exists(args[1]))
                {
                    Console.Error.WriteLine("Cannot submit non-existent file '{0}'", args[1]);
                    Environment.Exit(-3);
                }
                else
                {
                    var file = new FileInfo(args[1]);
                    var token = await api.SubmitFileAsync(file, file.Name, "win7");
                    Console.Out.WriteLine("Polling token: {0}", token);
                }
            }

            else if (args[0].Equals(RETRIEVE_FLAG))
            {
                var regex = @"[A-Fa-f0-9]{64}";
                var match = Regex.Match(args[1], regex, RegexOptions.IgnoreCase);

                if (!match.Success)
                {
                    Console.Error.WriteLine("Please check your file token '{0}'. It doesn't look right.", args[1]);
                    Environment.Exit(-4);
                }
                else
                {
                    var slimReport = true;
                    if(args.Length == 3)
                    {
                        if(args[2].Equals(FULL_REPORT_FLAG))
                        {
                            slimReport = false;
                        }
                    }
                    var report = await api.GetFileReportAsync(args[1], "json", "win7", slimReport);
                    var reportOnDisk = $"{args[1]}.json";
                    using (var writer = new StreamWriter(reportOnDisk, false))
                    {
                        writer.Write(report);
                    }
                    Console.Out.WriteLine("Report written to {0}", reportOnDisk);
                }
            }
        }
    }
}
