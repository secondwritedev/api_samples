﻿using System;
namespace SecondWriteApi.Objects
{
    public class Info
    {
        public Machine Machine { set; get; }

        public string Result { get; set; }
        public string ResultDescription { get; set; }
        public int MalwareConfidence { get; set; }
    }
}
