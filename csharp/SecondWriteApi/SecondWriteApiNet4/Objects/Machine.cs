﻿using System;
namespace SecondWriteApi.Objects
{
    public class Machine
    {
        public DateTime ShutdownOn { get; set; }
        public DateTime StartedOn { get; set; }
    }
}
