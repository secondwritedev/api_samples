﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SecondWriteApi.Exceptions;
using SecondWriteApi.Objects;

namespace SecondWriteApi
{
    public class SecondWrite
    {
        readonly Dictionary<string, string> _defaultValues;
        readonly JsonSerializer _serializer;
        readonly HttpClientHandler _httpClientHandler;
        readonly HttpClient _client;

        public long FileSizeLimit { get; set; } = 31457280; //30 MB
        private const int TimeOutInMinutes = 5;

        public SecondWrite(string apiKey, bool debug = false)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentException("You must supply a private API key", nameof(apiKey));

            if (apiKey.Length != 64)
                throw new ArgumentException("API key is malformed", nameof(apiKey));

            _defaultValues = new Dictionary<string, string>(1)
            {
                { "api_key", apiKey }
            };


            _serializer = JsonSerializer.Create();
            _serializer.NullValueHandling = NullValueHandling.Ignore;

            var apiUrl = "https://api.secondwrite.com/";

            _httpClientHandler = new HttpClientHandler
            {
                AllowAutoRedirect = true
            };


            _client = new HttpClient(_httpClientHandler)
            {
                BaseAddress = new Uri(apiUrl),
                Timeout = TimeSpan.FromMilliseconds(1000 * 60 * TimeOutInMinutes)
            };
        }

        public Task<string> SubmitFileAsync(FileInfo file, string fileName, string os)
        {
            if (!file.Exists)
                throw new FileNotFoundException("Could not find file", file.Name);

            if (string.IsNullOrWhiteSpace(os))
                throw new ArgumentException("Operating system is required");

            var values = new Dictionary<string, string>
            {
                { "os", os }
            };

            return SubmitFileAsync(file.OpenRead(), fileName, os);
        }

        public Task<string> SubmitFileAsync(byte[] file, string fileName, string os)
        {
            return SubmitFileAsync(new MemoryStream(file), fileName, os);
        }

        public Task<string> SubmitFileAsync(Stream stream, string fileName, string os)
        {
            if (stream == null)
                throw new ArgumentNullException(nameof(stream), "You must provide a stream that is not null");

            if (stream.Length <= 0)
                throw new ArgumentException("You must provide a stream with content", nameof(stream));

            if (stream.Length > FileSizeLimit)
                throw new SizeLimitException(FileSizeLimit, stream.Length);

            if (string.IsNullOrWhiteSpace(os))
                throw new ArgumentException("You must provide an operating system.");

            var multi = new MultipartFormDataContent
            {
                CreateApiPartOfMultiPart(),
                CreateResubmitPartOfMultiPart(true),
                CreateOsPartOfMultiPart(os),
                CreateFilePartOfMultiPart(stream, fileName)
            };

            return GetStringResult("submit", HttpMethod.Post, null, multi);
        }

        public async Task<string> GetFileReportAsync(string uuid, string format, string os, bool slim)
        {
            if (string.IsNullOrWhiteSpace(uuid))
                throw new InvalidResourceException("Resource is invalid");

            var values = new Dictionary<string, string>
            {
                { "sample", uuid },
                { "format", format },
                { "os", os }
            };

            string reportInfo = null;
            if (slim)
            {
                reportInfo = await GetStringResult("slim_report", HttpMethod.Get, new FormUrlEncodedContent(_defaultValues.Concat(values)), null);
            }
            else
            {
                reportInfo = await GetStringResult("report", HttpMethod.Get, new FormUrlEncodedContent(_defaultValues.Concat(values)), null);
            }

            return reportInfo;
        }

        async Task<T> GetResult<T>(string url, HttpMethod method, FormUrlEncodedContent queryParams, HttpContent content)
        {
            var response = await SendRequest(url, method, queryParams, content).ConfigureAwait(false);

            using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
            using (var sr = new StreamReader(responseStream, Encoding.UTF8))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                jsonTextReader.CloseInput = false;
                return _serializer.Deserialize<T>(jsonTextReader);
            }
        }

        async Task<string> GetStringResult(string url, HttpMethod method, FormUrlEncodedContent queryParams, HttpContent content)
        {
            var response = await SendRequest(url, method, queryParams, content).ConfigureAwait(false);

            using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
            using (var sr = new StreamReader(responseStream, Encoding.UTF8))
            {
                return sr.ReadToEnd();
            }
        }

        async Task<HttpResponseMessage> SendRequest(string path, HttpMethod method, FormUrlEncodedContent queryParams, HttpContent content)
        {
            var urlBuilder = new UriBuilder(_client.BaseAddress + path);

            if (queryParams != null)
            {
                urlBuilder.Query = await queryParams.ReadAsStringAsync();
            }

            var request = new HttpRequestMessage(method, urlBuilder.ToString());

            if (content != null)
            {
                request.Content = content;
            }

            var response = await _client.SendAsync(request).ConfigureAwait(false);

            if ((int)response.StatusCode == 429)
                throw new RateLimitException("Weekly limit reached");

            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                throw new ResultNotYetReadyException("Result not yet ready");

            if (response.StatusCode == HttpStatusCode.Forbidden)
                throw new AccessDeniedException("No user found by that key");

            if (response.StatusCode == HttpStatusCode.PaymentRequired)
                throw new ApiKeyHasExpired("Key has expired");

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
                throw new Exception($"API gave error code {response.StatusCode} ({(int)response.StatusCode})");

            return response;
        }

        HttpContent CreateApiPartOfMultiPart()
        {
            var content = new StringContent(_defaultValues["api_key"]);
            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"api_key\""
            };

            return content;
        }

        HttpContent CreateResubmitPartOfMultiPart(bool resubmit)
        {
            var content = new StringContent(resubmit.ToString());
            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"resubmit\""
            };

            return content;
        }

        HttpContent CreateOsPartOfMultiPart(string os)
        {
            var content = new StringContent(os);
            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"os\""
            };

            return content;
        }

        HttpContent CreateFilePartOfMultiPart(Stream stream, string fileName)
        {
            var fileContent = new StreamContent(stream);
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"file\"",
                FileName = "\"" + fileName + "\"",
                Size = stream.Length
            };
            fileContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return fileContent;
        }
    }
}
