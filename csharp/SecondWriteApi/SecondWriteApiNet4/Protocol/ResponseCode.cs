﻿using System;
namespace SecondWriteApi.Protocol
{
    public enum ResponseCode
    {
        Queued = -2,
        InvalidQuery = -1,
        NotFound = 0,
        Success = 1
    }
}
