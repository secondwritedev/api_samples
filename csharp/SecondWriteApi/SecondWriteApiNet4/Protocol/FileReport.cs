﻿using System;
using Newtonsoft.Json;

namespace SecondWriteApi.Protocol
{
    public class FileReport
    {
        public string MD5 { get; set; }
        [JsonProperty("response_code")]
        public ResponseCode ResponseCode { get; set; }
    }
}
