﻿using System;
namespace SecondWriteApi.Exceptions
{
    public class RateLimitException : Exception
    {
        public RateLimitException(string message)
            : base(message) { }
    }
}
