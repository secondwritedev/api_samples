﻿using System;
namespace SecondWriteApi.Exceptions
{
    public class ResultNotYetReadyException : Exception
    {
        public ResultNotYetReadyException(string message)
            : base(message) { }
    }
}
