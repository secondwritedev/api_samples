﻿using System;
namespace SecondWriteApi.Exceptions
{
    public class ApiKeyHasExpired : Exception
    {
        public ApiKeyHasExpired(string message)
            : base(message) { }
    }
}
