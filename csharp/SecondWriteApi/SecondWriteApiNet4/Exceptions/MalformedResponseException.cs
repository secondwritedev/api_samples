﻿using System;
namespace SecondWriteApi.Exceptions
{
    public class MalformedResponseException : Exception
    {
        public MalformedResponseException(string message)
            : base(message) { }
    }
}
