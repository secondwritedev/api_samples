﻿using System;
namespace SecondWriteApi.Exceptions
{
    public class InvalidResourceException : Exception
    {
        public InvalidResourceException(string message)
            : base(message) { }
    }
}
