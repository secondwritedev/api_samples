﻿using System;
namespace SecondWriteApi.Exceptions
{
    public class SizeLimitException : Exception
    {
        public SizeLimitException(long apiLimitBytes, long actualBytes)
            : base($"The file size limit on SecondWrite is {apiLimitBytes} bytes. Your file is {actualBytes} bytes") { }

        public SizeLimitException(long apiLimitBytes)
            : base($"The file size limit on SecondWrite is {apiLimitBytes} bytes.") { }
    }
}
